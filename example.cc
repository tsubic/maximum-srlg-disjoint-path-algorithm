#include "cupwns.h"

#include <cstdio>

#include <stdlib.h>

#include <time.h>

#define MAX(a, b) ((a > b) ? (a) : (b)) //macro for max of two values

void
readFromFileHost(int **&graph, int &n, int &e)
{
  int u, v;
  float w;

  //read from file
  FILE* fin = fopen("test.txt", "r");
  fscanf(fin, "%d", &e);
  //graph = (int*) (malloc(3 * e * sizeof));
  graph = new int*[e];
  n = -1;
  //for (int i = 0; i < (3 * e); i = i + 3)
  for (int i = 0; i < e; i++)
    {
      graph[i] = new int[3];
      fscanf(fin, "%d %d %f", &u, &v, &w);
      graph[i][0] = u;
      graph[i][1] = v;
      graph[i][2] = 1000 * w;
      n = MAX( u, MAX( v, n));
      printf("Graph source %d destionation %d weight %d\n", graph[i][0], graph[i][1], graph[i][2]);
    }
   n++; //iz important
}

int
main ()
{
  //read from file
  int **graph;  //vrijednosti iz datoteke
  int n; /* The number of nodes in the graph */
  int e; /* The number of nonzero edges in the graph */
  readFromFileHost(graph, n, e);

  //  for (int i = 0; i < e; i++)
  //    printf("%f %f %f\n", graph[i][0], graph[i][1], graph[i][2]);

  srand(time(NULL));

  int source = 0; // rand() % (n - 1) + 1;
  int destination = 4; // rand() % (n - 1) + 1;

  printf("Finding path from source %d to destination %d\n", source, destination);
  printf("Number of nodes %d\n", n);
 
  int *solution;
  int **eliminatedEdges;
  int solutionLen;
  CUDAPwnsPwnage(source, destination, graph, n, e, eliminatedEdges, 0, solution, solutionLen);

  delete[] solution;
  return 0;
}
