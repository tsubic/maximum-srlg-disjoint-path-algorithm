/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 University of Rijeka
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Tomislav Šubić <tomislav.subic@gmail.com>
 *          Vedran Miletić <rivanvx@gmail.com>
 */

#include <cstdio>
#include <cmath>

#define COL 3
//#define __CUPWNS_DEBUG__
//#define __CUPWNS_SOLUTION__
#define __CUPWNS_INFINITY__ (__INT32_MAX__ / 100)

#define THREADS_PER_BLOCK 512

void
print1dArrayHost(int* arr, int len, int lineBreakEachN = 0)
{
  for (int i = 0; i < len; i++)
    {
      if(arr[i]!=-1)
        printf("%d ", arr[i]);
      if(lineBreakEachN != 0 && i % lineBreakEachN == lineBreakEachN - 1)
        printf("\n");
      else if(lineBreakEachN != 0)
        printf("\t");
    }
  printf("\n");
}

void
print2dArrayHost(int** arr, int len1, int len2)
{
  for(int i = 0; i < len1;i++)
    {
      for(int j = 0; j < len2; j++)
        printf("%d ", arr[i][j]);
      printf("\n");
    }
}

__device__ void
pwnsDijkstraPrintPathDevice(int destination, int*& prev, int*& ss_paths, int n, int c)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  if (destination >= 0 && destination < n && prev[destination] != -1)
    {
      pwnsDijkstraPrintPathDevice(prev[destination], prev, ss_paths, n, c - 1);
    }
#ifdef __CUPWNS_DEBUG__
  printf("IDX=%d %d",threadIdx.x, c);
#endif
  ss_paths[idx * n + c] = destination;
}

__global__ void
pwnsDijkstraKernel(int source, int destination, int *graph, int dim, int n, int e, int *path, int *ss_paths, int t_count)
{
  int idx = threadIdx.x + blockDim.x * blockIdx.x;
  int crntSub2 = exp2f(t_count);
  int i, j, k, mini;

  const int gangstaN = 100;
  int visited[gangstaN]; //  visited = (int*)malloc(n*sizeof(int));
  int dist[gangstaN * gangstaN];/* dist[i][j] is the distance between node i and j; or 0 if there is no direct connection */
  int d[gangstaN];/* d[i] is the length of the shortest path between the source (s) and node i */
  int prev[gangstaN];/* prev[i] is the node that comes right before i in the shortest path from the source to i*/

  for (i = 0; i < n; i++)
    ss_paths[idx * n + i] = -1;

  //  printf("Idx %d dist %d\n", idx, dist);
  for (i = 0; i < n * n; i++)
    {
      //printf("Idx %d i %d n*n %d\n", idx, i, n*n);
      dist[i] = 0;
    }
 
  //loading graph data
  for (i = 0; i < COL * e; i = i + COL)
    {
      dist[graph[i] * n + graph[i + 1]] = graph[i + 2];
      dist[graph[i] + n * graph[i + 1]] = graph[i + 2];
#ifdef __CUPWNS_DEBUG__
      printf("Dist graph index 1 is %d\n", graph[i] * n + graph[i + 1]);
      printf("Dist graph index 2 is %d\n", graph[i] + n * graph[i + 1]);
#endif
     // dist[idx * n*n + (graph[i] * n + graph[i + 1])] = graph[i + 2];
     // dist[idx * n*n + (graph[i] + n * graph[i + 1])] = graph[i + 2];
    }

  //loading extra graph data specific for each thread
  
  for (i = 0; i < (COL * t_count * crntSub2); i += COL)
    {
      if (((idx * COL * t_count) <= i) && (i < ((idx + 1) * COL * t_count)))
        { 
          dist[path[i] * n + path[i + 1]] = path[i + 2];
          dist[path[i] + n * path[i + 1]] = path[i + 2];
#ifdef __CUPWNS_DEBUG__
          printf("Dist path index 1 is %d\n", path[i] * n + path[i + 1]);
          printf("Dist path index 2 is %d\n", path[i] + n * path[i + 1]);
          printf("Dist n*n is %d\n", n*n);
#endif
         // dist[idx * n*n + (path[i] * n +  path[i + 1])] = path[i + 2];
         // dist[idx * n*n + (path[i] + n *  path[i + 1])] = path[i + 2];
        }
    }

    /*
    if(idx==0){
    printf("GRAPH:\n");
   for(i=0;i<n;i++){
   for(j=0;j<n;j++)
   printf("%d ", dist[i*n+j]);
   printf("\n");
   }}
   return;*/
  //DIJSKTRA
  for (i = 0; i < n; ++i)
    {
      d[i] = __CUPWNS_INFINITY__;
      prev[i] = -1; // no path has yet been found to i 
      visited[i] = 0; // the i-th element has not yet been visited 
    }

  d[source] = 0;

  for (k = 0; k < n; ++k)
    {
      mini = -1;
      for (i = 0; i < n; ++i)
        {
          if (!visited[i] && (mini == -1 || (mini >= 0 && d[i] < d[mini])))
            mini = i;
        }

      if (mini >= 0)
        {
          visited[mini] = 1;
          for (i = 0; i < n; ++i)
            {
              if (dist[mini * n + i])
                {
                  if (d[mini] + dist[mini * n + i] < d[i])
                    {
                      d[i] = d[mini] + dist[mini * n + i];
                      prev[i] = mini;
                    }
                }
            }
        }
    }
  //return;
  //pwnsDijkstraPrintPathDevice(destination, prev, ss_paths, n, n - 1);
  for (j = n - 1; j >= 0; j--)
    {
      ss_paths[idx * n + j] = destination;
      if (destination >= 0 && destination < n)
        {
          destination = prev[destination];
        }
      else
        {
          break;
        }
    }

  //delete[] visited;
  //delete[] dist;
  //delete[] d;
  //delete[] prev;
  //for(i=0;i<n;i++)
  // printf("%d ", prev[i]);
}

void
savePathHost(int destination, int *&fsp, int *prev, int c)
{
  if (prev[destination] != -1)
    {
      savePathHost(prev[destination], fsp, prev, c - 1);
    }
  fsp[c] = destination;

//#ifdef __CUPWNS_DEBUG__
  printf("FSP[c]: %d\n", fsp[c]);
//#endif
}

void
computeFirstShortestPathHost(int s, int* graph, int n, int e, int*& first_shortest_path, int*& prev)
{
  //DIJSKTRA CPU - FIRST SHORTEST PATH-
  for (int i = 0; i < n; i++)
    first_shortest_path[i] = -1;
  int mini;
  int* visited = new int[n];
  int* dist = new int[n * n];
  int* d = new int[n];
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      dist[i * n + j] = 0;

  //loading graph distances
  for (int i = 0; i < COL * e; i = i + COL)
    {
      dist[ graph[i] * n +  graph[i + 1]] = graph[i + 2];
      dist[ graph[i] + n *  graph[i + 1]] = graph[i + 2];
    }

#ifdef __CUPWNS_DEBUG__
  printf("Distance array ");
  print1dArrayHost(dist, n * n);
#endif
  
  for (int i = 0; i < n; ++i)
    {
      d[i] = __CUPWNS_INFINITY__;
      prev[i] = -1; // no path has yet been found to i 
      visited[i] = 0; // the i-th element has not yet been visited 
    }
  d[s] = 0;
  for (int i = 0; i < n; ++i)
    {
      mini = -1;
      for (int j = 0; j < n; ++j)
        {
          if (!visited[j] && ((mini == -1) || (d[j] < d[mini])))
            mini = j;
        }
      visited[mini] = 1;
      for (int j = 0; j < n; ++j)
        {
          if (dist[mini * n + j])
            {
              if (d[mini] + dist[mini * n + j] < d[j])
                {
                  d[j] = d[mini] + dist[mini * n + j];
                  prev[j] = mini;
                }
            }
         }
      }
  delete[] visited;
  delete[] dist;
  delete[] d;
#ifdef __CUPWNS_DEBUG__
  printf("Dijkstra distance ");
  print1dArrayHost(d, n);
  printf("Dijkstra previous ");
  print1dArrayHost(prev, n);
#endif
}

int
computeThreadCountHost(int* first_shortest_path, int len)
{
  int threadCount=0;

  for (int i = 0; i < len; i++)
    if (first_shortest_path[i] != -1)
      threadCount++;
  threadCount--;
  //threadCount = pow(2, threadCount);

  return threadCount;
}

void
doSomethingSourcePathsHost(int** source, int* graph, int n, int* firstShortestPath, int threadCount)
{
  //odvajanje u novo polje s putevima
  for (int j = 0, i = 0; i < n && j < threadCount; i++)
    if (firstShortestPath[i] != -1)
      {
        source[j][0] = firstShortestPath[i];
        source[j][1] = firstShortestPath[i + 1];
        source[j][2] = 0;
        j++;
      }
}

void eliminateFromGraph (int *graph, int e, int**edges, int threadCount)
{
  for (int i = 0; i < COL * e; i += COL)
    {
      for (int j = 0; j < threadCount; j++)
        {
          if (graph[i] == edges[j][0] && graph[i + 1] == edges[j][1] || graph[i] == edges[j][1] && graph[i + 1] == edges[j][0])
            {
              edges[j][2] = graph[i + 2];
              graph[i + 2] = 0;
            }
        }
    }
}

void
makeArrayFlat(int** arr, int length, int *&flatArr)
{
  for (int i = 0; i < length; i++)
    for (int j = 0; j < COL; j++)
      {
        flatArr[i * COL + j] = arr[i][j];
        //printf("Flat array %d %d ima vrijednost %d\n", i, j, flatArr[i * COL + j]);
      }
}

int
arraySubsettingHost(int** source, int *&flat_subset, int threadCount)
{
  int currentSubset = pow(2, threadCount) - 1;
  int currentSubsetPlusOne = currentSubset + 1;

  // svaka kombinacija ima threadCount elemenata od po 3 int, a kombinacija je pow(2, threadCount)
  int** subset = new int*[threadCount * currentSubsetPlusOne];
  for (int i = 0; i < (threadCount * currentSubsetPlusOne); i++)
    subset[i] = new int[COL];
  for (int i = 0; i < (threadCount * currentSubsetPlusOne); i++)
    {
      for (int j = 0; j < COL; j++)
        {
          subset[i][j] = 0;
        }
    }
  int tmp;
  while (currentSubset != 0)
    {
      tmp = currentSubset;
      for (int i = 0; i < threadCount; i++)
        {
          if (tmp & 1)
            {
              //printf("%d %d %d ", source[i][0], source[i][1], source[i][2]); //writes out everything I want
              subset[currentSubset * threadCount + i][0] = source[i][0];
              subset[currentSubset * threadCount + i][1] = source[i][1];
              subset[currentSubset * threadCount + i][2] = source[i][2];
            }
          tmp >>= 1;
        }
      currentSubset--;
    }
  makeArrayFlat(subset, threadCount * currentSubsetPlusOne, flat_subset);
  for (int i = 0; i < (threadCount * currentSubsetPlusOne); i++)
    delete[] subset[i];
  delete[] subset;
  return currentSubsetPlusOne;
}

void
CUDAPwnsPwnage(int s, int d, int** graph, int n, int e, int** eliminatedEdges, int nEliminatedEdges, int *&solution, int& solutionLen)
{
  int* flatGraph = new int[COL * e];
  makeArrayFlat(graph, e, flatGraph);
#ifdef __CUPWNS_DEBUG__
  printf("Graph\n");
  print1dArrayHost (flatGraph, COL * e, COL);
#endif

  //array subset part
  int* flatSubset;
  int currentSubsetPlusOne;
  int threadCount;
  if (nEliminatedEdges == 0)
    {
      //compute first shortest path on CPU
      int* prev = new int[n]; //Dijkstra previous
      int *firstShortestPath = new int[n]; //prvi najkraci put
#ifdef __CUPWNS_DEBUG__
      printf("Starting host Dijkstra\n");
 #endif
      computeFirstShortestPathHost(s, flatGraph, n, e, firstShortestPath, prev);
      savePathHost(d, firstShortestPath, prev, n - 1);
#ifdef __CUPWNS_SOLUTION__
      printf("First shortest path\n");
      print1dArrayHost(firstShortestPath, n);
#endif

      //compute number of threads
      threadCount = computeThreadCountHost(firstShortestPath, n);
      
       //odvajanje u novo polje s putevima
      int** source = new int*[threadCount];
        for (int i = 0; i < threadCount; i++)
          source[i] = new int[COL];
      doSomethingSourcePathsHost(source, flatGraph, n, firstShortestPath, threadCount);
#ifdef __CUPWNS_DEBUG__
      print2dArrayHost(source, threadCount, COL);
#endif
      eliminateFromGraph(flatGraph, e, source, threadCount);

      flatSubset = new int[int(pow(2, threadCount) * threadCount * COL)];
      currentSubsetPlusOne = arraySubsettingHost(source, flatSubset, threadCount);
      delete[] prev;
      delete[] firstShortestPath;
      /*for (int i = 0; i < threadCount; i++)
        free(source[i]);
      free(source);*/
      delete[] source;
    }
  else
    {
      threadCount = nEliminatedEdges;
      eliminateFromGraph(flatGraph, e, eliminatedEdges, threadCount);
#ifdef __CUPWNS_SOLUTION__
      printf("Number of eliminated edges %d\n", nEliminatedEdges);
      for(int i = 0; i < nEliminatedEdges; i++)
        printf("Eliminated edge from %d to %d weight %d\n", eliminatedEdges[i][0], eliminatedEdges[i][1], eliminatedEdges[i][2]);
#endif
      flatSubset = new int[int(pow(2, threadCount) * threadCount * COL)];
      currentSubsetPlusOne = arraySubsettingHost(eliminatedEdges, flatSubset, threadCount);
    }

#ifdef __CUPWNS_DEBUG__
  print1dArrayHost(flatSubset, COL * threadCount * currentSubsetPlusOne, COL * threadCount);
#endif

  // allocate on host
  solution = new int[currentSubsetPlusOne * n]; //ostali najkraci putevi
  solutionLen = currentSubsetPlusOne * n;

  //allocate on device
  int *dev_graph;
  int *dev_fsp;
  int *dev_ss_paths;

  cudaMalloc((void**) &dev_graph, COL * e * sizeof(int));
  cudaMalloc((void**) &dev_fsp, COL * threadCount * currentSubsetPlusOne * sizeof(int));
  cudaMalloc((void**) &dev_ss_paths, currentSubsetPlusOne * n * sizeof(int));

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("0Error: %s\n", cudaGetErrorString(err));

  //copy to device
  cudaMemcpy(dev_graph, flatGraph, COL * e * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(dev_fsp, flatSubset, COL * threadCount * currentSubsetPlusOne * sizeof(int), cudaMemcpyHostToDevice);

  err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("1Error: %s\n", cudaGetErrorString(err));

  int blockCount = 1;
  int realThreadCount = currentSubsetPlusOne;
  if (pow(2,threadCount)>THREADS_PER_BLOCK)
    {
      blockCount = pow(2,threadCount) / THREADS_PER_BLOCK;
      realThreadCount = currentSubsetPlusOne / blockCount;
    }
  /*
  //allocate dist on host
  int *host_dist = new int[blockCount*realThreadCount*n*n];
  for (int i = 0; i < blockCount*realThreadCount*n*n; i++)
    host_dist[i] = 0; 

  //allocate on device
  int *dev_dist;
  cudaMalloc((void**) &dev_dist, blockCount*realThreadCount*n*n * sizeof(int));

  err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("2Error: %s\n", cudaGetErrorString(err));

  cudaMemcpy(dev_dist, host_dist, blockCount*realThreadCount*n*n * sizeof(int), cudaMemcpyHostToDevice);

  err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("3Error: %s\n", cudaGetErrorString(err));

  //delete[] host_dist;
*/
  int dim = blockCount * realThreadCount;
  printf("Run configuration %d blocks %d thread\n", blockCount, realThreadCount);
  pwnsDijkstraKernel<<<blockCount, realThreadCount>>>(s, d, dev_graph, dim, n, e, dev_fsp, dev_ss_paths, threadCount);
  cudaDeviceSynchronize();

  err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("4Error: %s\n", cudaGetErrorString(err));

  cudaMemcpy(solution, dev_ss_paths, currentSubsetPlusOne * n * sizeof(int), cudaMemcpyDeviceToHost);
  
  err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("5Error: %s\n", cudaGetErrorString(err));

  cudaFree(dev_graph);
  cudaFree(dev_fsp);
  //cudaFree(dev_dist);
  cudaFree(dev_ss_paths);

  err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("6Error: %s\n", cudaGetErrorString(err));

  delete[] flatGraph;
  delete[] flatSubset;

#ifdef __CUPWNS_SOLUTION__
  printf("\nSvi putevi za sve podskupove bridova najkraceg puta\n");

  for (int i = 0; i < currentSubsetPlusOne; i++)
    {
      for (int j = 0; j < n; j++)
        {
          if(solution[i * n + j] != -1)
            printf("%d ", solution[i * n + j]);
        }
      printf("\n");
    }
#endif
}
