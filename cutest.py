import numpy as np
import subprocess as sp
import os

#number of tests
n = 1000
#number of edges
edge = 200
col = 3 #constant
#number of nodes
node = 70

error = "Error"

for i in range(n):
    dist = np.random.rand(edge,col)
    dat = open("test.txt","w")
    dat.write(str(edge)+"\n")
    for row in dist:
        row[0] = int((row[0] * 1000) % node)
        row[1] = int((row[1] * 1000) % node)
        row[2] = row[2] * 100
    dist = dist.astype(int)
    #print (dist)
    for row in dist:
        dat.write(str(row[0]) + " " + str(row[1]) + " " + str(row[2]) + "\n")
    dat.close()
    a = sp.Popen(["./a.out"], stdout=sp.PIPE)
    (out, err) = a.communicate()
    line = str(out) + str(err)
    if error in line:
	print(line)
        print ("==> FAIL <==")
    else:
        #print("==> PASS <==")
	pass
