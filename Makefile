# --- targets
all:  cupwns.cu cupwns.h 
	nvcc -O3 -g -arch=sm_20 -c -Xcompiler -fPIC cupwns.cu
	nvcc -shared -o libcupwns.so cupwns.o
	
#-- test program
#	g++ main.cc -g -o test -L. -lfwrouter
	
clean:
	rm -f cupwns.o libcupwns.so #test
